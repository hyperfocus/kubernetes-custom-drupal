ZONE=asia-east1-a

# Bootstrap
deploy: create-disk register-disk create-drupal

create-disk:
	gcloud compute disks create --size=20GB --zone=${ZONE} drupal-1

register-disk:
	kubectl create -f gce-volumes.yaml

create-drupal:
	kubectl create -f drupal-deployment.yaml


# Info
list-pods:
	kubectl get pods -l app=drupal

describe-drupal:
	kubectl describe deployment drupal

describe-pv:
	kubectl describe pv drupal-pv-1

describe-pv-claim:
	kubectl describe pvc dp-pv-claim


# Update
update: update-drupal

update-drupal:
	kubectl apply -f drupal-deployment.yaml


# Delete
delete: delete-deployment delete-pvc delete-pv delete-disk

delete-deployment:
	kubectl delete -f drupal-deployment.yaml

delete-pvc:
	kubectl delete pvc dp-pv-claim

delete-pv:
	kubectl delete pv drupal-pv-1

delete-disk:
	gcloud compute disks delete drupal-1
